FROM node:lts-alpine
RUN mkdir -p /api
WORKDIR /api
ENV NODE_ENV=dev
COPY ./ ./
RUN apk --update add tzdata
RUN cp /usr/share/zoneinfo/America/Mexico_City /etc/localtime
RUN echo 'America/Mexico_City' > /etc/timezone && apk del tzdata
RUN npm install
EXPOSE 3000
RUN npm run build
CMD [ "npm", "run", "serve" ]