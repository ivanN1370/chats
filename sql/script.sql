create database "social-network";

create schema "sn-schema";

create type status as enum ('ON', 'OFF');

alter type status owner to postgres;

create table users
(
    uid         varchar                                              not null
        constraint users_pk
            primary key,
    email       varchar(50)                                          not null,
    status      "sn-schema".status default 'OFF'::"sn-schema".status not null,
    blacklisted character varying[],
    created_at  timestamp          default now(),
    updated_at  timestamp
);

alter table users
    owner to postgres;

create unique index users_email_uindex
    on users (email);

create unique index users_uid_uindex
    on users (uid);

create table rooms
(
    room_id       varchar                 not null
        constraint rooms_pk
            primary key,
    participant_1 varchar                 not null
        constraint rooms_users_uid_fk
            references users,
    participant_2 varchar                 not null
        constraint rooms_users_uid_fk_2
            references users,
    created_at    timestamp default now() not null,
    updated_at    timestamp
);

alter table rooms
    owner to postgres;

create table messages
(
    message_id varchar(36)             not null
        constraint messages_pk
            primary key,
    room_id    varchar                 not null
        constraint messages_rooms_room_id_fk
            references rooms,
    "from"     varchar                 not null
        constraint messages_users_uid_fk
            references users,
    "to"       varchar                 not null
        constraint messages_users_uid_fk_2
            references users,
    message    varchar                 not null,
    created_at timestamp default now() not null,
    updated_at timestamp
);

alter table messages
    owner to postgres;

