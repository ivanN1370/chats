import { Sequelize } from 'sequelize';

export const db: Sequelize = new Sequelize('social-network', 'postgres', 'postgres123', {
  port: 5432,
  host: 'localhost',
  dialect: 'postgres',
  //logging: false,
});