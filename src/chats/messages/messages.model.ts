import { Model, ModelCtor, STRING, UUIDV4 } from 'sequelize';
import { v4 } from 'uuid';
import { db } from '../../db/pg';

export interface IMessage {
  message_id?: string;
  room_id?: string;
  uid?: string;
  from?: string;
  ts?: string;
  to?: string;
  message?: string;
}

export class Message {
  constructor({ room_id, from, to, message }: IMessage) {
    this.room_id = room_id;
    this.from = from;
    this.to = to;
    this.message = message;
  }
  private room_id: string;
  private from: string;
  private to: string;
  private message: string;
  public async saveMessage(): Promise<{ code: number, data: any }> {
    try {
      const message = await this.messageEntity().create({
        message_id: v4(),
        room_id: this.room_id,
        from: this.from,
        to: this.to,
        message: this.message,
      });
      return { code: 201, data: message };
    } catch (error) {
      return { code: 500, data: error };
    }
  }

  public async getMessages() {

  }

  private messageEntity(): ModelCtor<Model<IMessage>> {
    return db.define('message', {
      message_id: {
        type: UUIDV4,
        primaryKey: true,
      },
      room_id: {
        type: STRING,
      },
      from: {
        type: STRING,
      },
      to: {
        type: STRING,
      },
      message: {
        type: STRING,
      }
    }, {
      tableName: 'messages',
      schema: 'sn-schema',
      underscored: true
    });
  }
}
