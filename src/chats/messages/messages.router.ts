import { Router } from 'express';
import ctrl from './messages.ctrl';

export const messagesRouter = Router();

messagesRouter
  .get('/', ctrl.getMessages)
  ;