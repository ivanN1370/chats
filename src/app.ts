import cors from 'cors';
import express, { Application } from 'express';
import { createServer } from 'http';
import morgan from 'morgan';
import { Server } from 'socket.io';
import socket from './socket';
import { mainRouter } from './main/main.router';

export const app: Application = express();
export const http = createServer(app);
export const io = new Server(http, { cors: { origin: '*' } });

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(morgan('short'));
app.use(cors());

io.on('connection', socket);


app.use('/api', mainRouter);
