import { NextFunction, Request, Response } from 'express';
import { auth } from './firebase.auth';

export const verifyToken = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { authorization } = req.headers;
    const token = String(authorization).replace('Bearer ', '');
    const { uid } = await auth.verifyIdToken(token);
    req.headers.uid = uid;
    next();
  } catch (error) {
    res.status(403).json({ message: 'Invalid token' });
  }
}

export const verifyTokenSocket = async (token: string): Promise<any> => {
  try {
    token.replace('Bearer ', '');
    const { uid } = await auth.verifyIdToken(token);
    return { valid: true, uid };
  } catch (error) {
    return { valid: false };
  }
}