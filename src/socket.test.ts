const io2 = require('socket.io-client');

(async () => {
  const socket = io2('http://localhost:3000', { auth: { token: 'eyJhbGciOiJSUzI1NiIsImtpZCI6Ijc3MTBiMDE3ZmQ5YjcxMWUwMDljNmMzNmIwNzNiOGE2N2NiNjgyMTEiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vcmF6b3ItYTJhM2IiLCJhdWQiOiJyYXpvci1hMmEzYiIsImF1dGhfdGltZSI6MTYyNjY3ODY2MCwidXNlcl9pZCI6Ijd5cUNsVnZSRkxjbnYxVXlSRXpEcWpGa1dmUDIiLCJzdWIiOiI3eXFDbFZ2UkZMY252MVV5UkV6RHFqRmtXZlAyIiwiaWF0IjoxNjI2Njc4NjYwLCJleHAiOjE2MjY2ODIyNjAsImVtYWlsIjoicHJ1ZWJhMkBjb3JyZW8uY29tIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJmaXJlYmFzZSI6eyJpZGVudGl0aWVzIjp7ImVtYWlsIjpbInBydWViYTJAY29ycmVvLmNvbSJdfSwic2lnbl9pbl9wcm92aWRlciI6InBhc3N3b3JkIn19.VKuBo61h9ZjdY1dyN_-led_T3RsoxWWn0w4VSVhtBawyGbjaWkS8uZH__Q4Ee_Kc0BXFdMXyhKZhBRSjmFk2DMQ5FljPfO69vVdxtToXAUVgOioHIdSjHUMlN4s3kzXDqhXtfthgCNPGI7OngjtwsHYsk4WvW11ruDmRHVKkUTV5nZwbwVsgCh6velbNgTs26CE3qRvNXYqSxEm84aCFe-ZVC1r5UKV2KELNxztMrWx36-BzjsDfvlr3nKQGAbdbvzqxuw-1R_LhOdy47siSS3giMMDHewx4aAMyQJzHL_r1xxgWo4OPSHQ1flDGS5ch6WG_IKx1Mtrcdpmd3O6fuQ' } });
  console.log('listening');
  const send = () => {
    setTimeout(() => {
      socket.emit('new-message', { room_id: 'fee0e1947bc1d3acde2b4c0d802c23fa0d58ea71ef2cb1051f7d1a0b10b11be4', to: 'i4BEwmddFsNDyzGALrgfgWitzPD3', message: 'Hola, mundo!' });
    }, 3000)
  }
  send();
})();