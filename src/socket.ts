import { Socket } from 'socket.io';
import { io } from './app';
import { verifyTokenSocket } from './auth/jwt';
import { Status, UsersModel } from './users/users.model';
import { IMessage, Message } from './chats/messages/messages.model';

const socket = async (socket: Socket) => {
  try {
    if (socket.handshake.auth.token) {
      const { valid, uid } = await verifyTokenSocket(socket.handshake.auth.token);
      const User = new UsersModel({ uid, status: Status.ON });
      if (valid) {
        User.SetStatus();
        socket.on('new-message', async (data: any) => {
          const message: IMessage = data;
          message.ts = new Date().toISOString();
          message.from = uid;
          const save = new Message(message);
          await save.saveMessage();
          io.emit(message.room_id, message.message);
        });
        socket.on('disconnect', async () => {
          User.status = Status.OFF;
          User.SetStatus();
        });
      } else {
        socket.disconnect();
      }
    } else {
      socket.disconnect();
    }
  } catch (error) {
    return error;
  }
}

export default socket;