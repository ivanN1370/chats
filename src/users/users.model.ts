import { ARRAY, ENUM, Model, ModelCtor, STRING, Op } from 'sequelize';
import { db } from '../db/pg';
import redis from '../db/redis';

export enum Status {
  'OFF',
  'ON'
}

interface IUser {
  uid: string;
  email: string;
  status?: string;
  blacklisted?: string[];
}

export class UsersModel {
  constructor({ uid, status }: { uid?: string; email?: string; status?: Status }) {
    this.uid = uid;
    this.status = status;
  }
  public uid: string;
  public email: string;
  public status: Status;
  public blacklisted: string[];

  public async AddUser(email: string) {
    try {
      this.email = email;
      const user = this.UserEntity();
      const add = await user.create({
        uid: this.uid,
        email: this.email,
      });
      return { code: 201, data: add };
    } catch (error) {
      return { code: 500, data: error };
    }
  }

  public async GetUsers(): Promise<{ code: number, data: any }> {
    try {
      const users = this.UserEntity();
      
      const getUsers = await users.findAll(
        {
          attributes: ['uid', 'email', 'status'],
          where: {
            uid: {
              [Op.not]: this.uid,
              [Op.notIn]: await this.GetBlackListed(),
            },
          }
        });
      return { code: 200, data: getUsers };
    } catch (error) {
      return { code: 500, data: error };
    }
  }

  public async GetBlackListed(): Promise<string[]> {
    try {
      const getBlackListed = await this.UserEntity().findOne(
        {
          attributes: ['blacklisted'],
          where: { uid: this.uid },
        });
      if (getBlackListed.get().blacklisted) {
        this.blacklisted = getBlackListed.get().blacklisted;
      }
      return this.blacklisted ? this.blacklisted : [];
    } catch (error) {
      return error;
    }
  }

  public async BlockUser(users: string[]): Promise<{ code: number, data: any }> {
    try {
      await this.GetBlackListed();
      for (const user of users) {
        this.blacklisted.push(user);
        this.blacklisted = [...new Set([...this.blacklisted])];
      }
      const blockUser = await this.UserEntity().update({
        blacklisted: this.blacklisted,
      }, { where: { uid: this.uid } });
      return { code: 204, data: blockUser };
    } catch (error) {
      return { code: 500, data: error };
    }
  }

  public async UnblockUser(users: string[]): Promise<{ code: number, data: any }> {
    try {
      await this.GetBlackListed();
      for (const user of users) {
        const item = this.blacklisted.indexOf(user);
        if (item !== -1) {
          this.blacklisted.splice(item, 1);
        }
      }
      const unblockUser = await this.UserEntity().update({
        blacklisted: this.blacklisted,
      }, { where: { uid: this.uid } });
      return { code: 204, data: unblockUser };
    } catch (error) {
      return error;
    }
  }

  public async SetStatus(): Promise<{ code: number, data: any }> {
    try {
      const updateStatus = await this.UserEntity().update({
        status: await this.set(),
      }, { where: { uid: this.uid } });
      return { code: 204, data: updateStatus };
    } catch (error) {
      return { code: 400, data: error };
    }
  }

  private async set(): Promise<Status | any> {
    try {
      if (this.status.valueOf()) {
        await redis.sadd('USERS_ONLINE', this.uid);
      } else await redis.srem('USERS_ONLINE', this.uid);
      return Status[this.status];
    } catch (error) {
      return error;
    }
  }

  public UserEntity(): ModelCtor<Model<IUser>> {
    return db.define('user', {
      uid: {
        type: STRING,
        primaryKey: true,
      },
      email: {
        type: STRING,
      },
      status: {
        type: ENUM('ON', 'OFF'),
        validate: {
          isIn: {
            args: [['ON', 'OFF']],
            msg: `Must be ON or OFF.`,
          }
        },
        defaultValue: 'OFF',
      },
      blacklisted: {
        type: ARRAY(STRING),
      }
    }, {
      tableName: 'users',
      schema: 'sn-schema',
      underscored: true
    });
  }
}