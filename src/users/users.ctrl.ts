import { Request, Response } from 'express';
import { UsersModel } from './users.model';

class UsersCtrl {
  public async addUser(req: Request, res: Response): Promise<void> {
    try {
      const { email } = req.body;
      const uid = req.headers.uid;
      const model = new UsersModel({ uid: String(uid) });
      const add = await model.AddUser(email);
      res.status(add.code).json({ data: add.data });
    } catch (error) {
      res.status(500).json(error);
    }
  }

  public async getUsers(req: Request, res: Response): Promise<void> {
    try {
      const uid = req.headers.uid;
      const model = new UsersModel({ uid: String(uid) });
      const get = await model.GetUsers();
      res.status(get.code).json({ data: get.data });
    } catch (error) {
      res.status(500).json(error);
    }
  }

  public async blockUser(req: Request, res: Response): Promise<void> {
    try {
      const uid = req.headers.uid;
      const { users } = req.body;
      const model = new UsersModel({ uid: String(uid) });
      const block = await model.BlockUser(users);
      res.status(block.code).json({ data: block.data });
    } catch (error) {
      res.status(500).json(error);
    }
  }

  public async unblockUser(req: Request, res: Response): Promise<void> {
    try {
      const uid = req.headers.uid;
      const { users } = req.body;
      const model = new UsersModel({ uid: String(uid) });
      const unblock = await model.UnblockUser(users);
      res.status(unblock.code).json({ data: unblock.data });
    } catch (error) {
      res.status(500).json(error);
    }
  }

}

export default new UsersCtrl();
